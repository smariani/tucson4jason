/*
 * Copyright 1999-2015 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of TuCSoN4Jason <http://tucson4jason.apice.unibo.it>.
 *
 *    TuCSoN4Jason is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    TuCSoN4Jason is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with TuCSoN4Jason.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
package it.unibo.tucson4jason.operations;

import java.util.List;
import alice.tuplecentre.api.Tuple;

/**
 * Interface that represents the datatype of the results of TuCSoN operations.
 *
 * @author Alessandro Fantini (mailto: alessandro.fantini3@studio.unibo.it)
 * @author Federico Foschini (mailto: federico.foschini4@studio.unibo.it)
 */
public interface TucsonResult {

    /**
     * Method used to get the result of a TuCSoN operation that returns a list
     * of Tuple.
     *
     * @return The list of Tuple, result of a TuCSoN operation.
     */
    List<Tuple> getTuples();

    /**
     * Method used to get the result of a TuCSoN operation that returns a Tuple.
     *
     * @return The Tuple, result of a TuCSoN operation.
     */
    Tuple getTuple();

    /**
     * Method used to ask if the result of a TuCSoN operation is a Tuple or a
     * list of Tuple.
     *
     * @return False if the result is a Tuple, true if the result is a list of
     *         Tuple.
     */
    boolean isList();

}
