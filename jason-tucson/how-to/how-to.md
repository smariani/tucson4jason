# TuCSoN4Jason "how-to"

In this brief "how-to", you will learn how to get TuCSoN4Jason (**t4jn** for short), build it and run a first example showcasing its features.

Assumptions are you are familiar with Java (compilation), Jason (usage), Git (cloning repositories) and, optionally, ANT (buildfiles).

---

1. <a href="#getting">Getting Started with t4jn</a>
   
   1.1 <a href="#downloading">Downloading</a>
   
   1.2 <a href="#compiling">Compiling</a>
   
   1.3 <a href="#deploying">Deploying</a>
   
   1.4 <a href="#running">Running</a>
      
2. <a href="#contact">Contact Information</a>

---

### 1. <a name="getting">Getting Started with t4jn</a>

---

###### 1.1 <a name="downloading">Downloading</a>

If you want the *ready-to-use* distribution of t4jn, download **t4jn.jar** archive from the "Downloads" section, here > <http://bitbucket.org/smariani/tucson4jason/downloads>.

If you want the *source code* of t4jn, clone the t4jn **[Git](http://git-scm.com) repository** hosted here on Bitbucket, at > <http://smariani@bitbucket.org/smariani/tucson4jason.git> (e.g., from a command prompt type `$> git clone https://smariani@bitbucket.org/smariani/tucson4jason.git`) <a href="#1">\[1\]</a>.

In the former case, skip to the "<a href="#running">Running</a>" section below. In the latter case, keep reading.

---

###### 1.2 <a name="compiling">Compiling</a>

By cloning t4jn you have downloaded a folder named `tucson4jason/`, with the following *directory structure*:

    tucson4jason/
    |__...
    |__jason-tucson/
       |__...
       |__ant-scripts/
          |__build.xml
          |__environment.properties
       |__eclipse-config/
       |__how-to/
       |__license-info/
       |__src/

t4jn depends on 3 other Java libraries to function properly <a href="#2">\[2\]</a>:

 * **Jason**, downloadable from Jason "Download" page, here > <http://sourceforge.net/projects/jason/files/> (**jason.jar**)
 * **TuCSoN**, downloadable from TuCSoN "Downloads" section, here > <http://bitbucket.org/smariani/tucson/downloads> (**tucson.jar**)
 * **tuProlog**, downloadable from tuProlog "Download" page, here > <http://apice.unibo.it/xwiki/bin/view/Tuprolog/Download> (**2p.jar**)
 
Once you got the above libraries, you are ready to compile t4jn source code. 

The easiest way to do so is by exploiting the [ANT](http://ant.apache.org) script named `build.xml` within folder `ant-scripts/`, which takes care of the whole building process for you, from compilation to deployment (covered in next section). To do so, you need to have ANT installed on your machine <a href="#3">\[3\]</a>. If you don't want to use ANT, build t4jn jar archive using the tools you prefer, then skip to the "<a href="#running">Running</a>" section below.

To compile t4jn using ANT:

 1. Edit the `environment.properties` file according to your system configuration:
 
    1.1 Tell ANT where your JDK and your `java` tool are
    
    1.2 Tell ANT which libraries are needed to compile t4jn (those you just downloaded, that is Jason, TuCSoN and tuProlog)
    
    1.3 Tell ANT where you put such libraries (e.g. if you put them into `jason-tucson/libs/` you are already set)
    
    *\[1.4 Tell ANT your Bitbucket username (for automatic syncing with t4jn repository, **not supported at the moment**)\]*
 
 2. Launch the ANT script using target `compile` (e.g., from a command prompt position yourself into the `ant-scripts/` folder then type `$> ant compile`) <a href="#4">\[4\]</a>. This will create folder `classes/` within folder `jason-tucson/` and therein store Java `.class` files.
 
Other ANT targets are available through the `build.xml` file: to learn which, launch the ANT script using target `help`.

---

###### 1.3 <a name="deploying">Deploying</a>

Deploying t4jn is as simple as giving a different build target to the ANT script `build.xml`:

 * if you only want the **t4jn jar** archive, ready to be included in your Jason project, launch the script using target `lib`. This will compile t4jn source code into binaries (put into `jason-tucson/classes/` folder) then package them to **t4jn.jar** into `jason-tucson/lib/` folder.
 
 * if you want a **ready-to-release distribution** of t4jn, including also documentation and support libraries, launch the script using target `dist`. This will:
   
   * compile t4jn source code into binaries, put into `jason-tucson/classes/` folder
   * package them to t4jn.jar, put into `jason-tucson/lib/` folder
   * generate Javadoc information, put into `jason-tucson/doc/` folder
   * create folder `rel/TuCSoN4Jason-${version}` including:
   
     * folder `docs/` including the generated Javadoc information as well as this "how-to"
     * folder `libs/` including Jason, TuCSoN and tuProlog libraries used to build t4jn
     * folder `rel/` including t4jn jar archives and example programs
     
The complete directory structure obtained by launching `ant dist` build process should look like the following (assuming you put Jason, TuCSoN and tuProlog libraries in folder `jason-tucson/libs/`):

    tucson4jason/
    |__...
    |__jason-tucson/
       |__...
       |__ant-scripts/
          |__build.xml
          |__environment.properties
       |__classes/
       |__doc/
       |__eclipse-config/
       |__how-to/
       |__rel/
          |__TuCSoN4Jason-${version}/
             |__docs/
                |__how-to/
                |__javadoc/
             |__libs/
             |__rel/
          |__...
       |__lib/
       |__libs/
       |__license-info/
       |__src/

Other ANT targets are available through the `build.xml` file: to learn which, launch the ANT script using target `help`.

---

###### 1.4 <a name="running">Running</a>

To run t4jn, you need:

 * t4jn jar, **t4jn.jar**
 * Jason jar, **jason.jar**
 * TuCSoN jar, **tucson.jar**
 * tuProlog jar, **2p.jar**

Supposing you built t4jn using the provided ANT script <a href="#5">\[5\]</a> and that you are comfortable with using a command prompt to launch Java applications <a href="#6">\[6\]</a>:

 1. open a command prompt and position yourself into `jason-tucson/`
 2. launch a TuCSoN node on the default address, as follows <a href="#7">\[7\]</a>:

        java -cp libs/tucson.jar:libs/2p.jar alice.tucson.service.TucsonNodeService

 3. open a new tab/window of the command prompt and position yourself into either `jason-tucson/lib/` or `jason-tucson/rel/TuCSoN4Jason-${version}/rel/` folder
 4. launch the example application "Book Trading" MAS (MAS configuration file  `t4jn_bookTrading.mas2j`), shipped within t4jn.jar, showcasing its features, as follows <a href="#7">\[7\]</a>:

        java -cp t4jn.jar:../libs/jason.jar:../libs/2p.jar:../libs/tucson.jar jason.infra.centralised.RunCentralisedMAS t4jn_bookTrading.mas2j

Jason GUI should appear with the t4jn welcome banner, as depicted below.

<img src="t4jn-running.jpeg" alt="t4jn in execution" width="600" height="380">
 
You should see many prints on Jason GUI, tracking what happens in the MAS.

---

### <a name="contact">Contact Information</a>

**Author** of this "how-to":

 * *Stefano Mariani*, DISI - Università di Bologna (<s.mariani@unibo.it>)

**Authors** of the library:

 * *Stefano Mariani*, DISI - Università di Bologna (<s.mariani@unibo.it>)
 * Federico Foschini, Università di Bologna
 * Alessandro Fantini, Università di Bologna
 * Prof. Andrea Omicini, DISI - Università di Bologna
 
---

<a name="1">\[1\]</a> Git standalone clients are available for any platform (e.g., [SourceTree](http://www.sourcetreeapp.com) for Mac OS and Windows). Also, if you are using [Eclipse IDE](http://www.eclipse.org/home/index.php) for developing in Jason, the [EGit plugin](http://marketplace.eclipse.org/content/egit-git-team-provider) is included in the [Java Developers version](http://www.eclipse.org/downloads/packages/eclipse-ide-java-developers/lunasr1) of the IDE.

<a name="2">\[2\]</a> Recommended Jason version is **1.4.2**. Regarding TuCSoN and tuProlog, recommended version is **1.12.0.0301** and **3.0.1**, respectively. Others (both newer and older) may work properly, but they have not been tested.

<a name="3">\[3\]</a> Binaries available [here](http://ant.apache.org/bindownload.cgi), installation instructions covering Linux, MacOS X, Windows and Unix systems [here](http://ant.apache.org/manual/install.html).

<a name="4">\[4\]</a> If you are using [Eclipse IDE](http://www.eclipse.org/home/index.php) for developing in Jason, ANT is included: click "Window > Show View > Ant" then click "Add buildfiles" from the ANT view and select file `build.xml` within `ant-scripts/` folder. Now expand the "TuCSoN4Jason build file" from the ANT view and finally double click on target `compile` to start the build process.

<a name="5">\[5\]</a> If you directly downloaded t4jn jar or if you built it from sources without using the provided ANT script, simply adjust the given command to suit your configuration.

<a name="6">\[6\]</a> If you do not want to use the command prompt to launch Jason applications, adjust the given command to suit your configuration, e.g., if your are using Eclipse IDE: right-click on "tucson.jar > Run As > Run Configurations..." then double-click on "Java Application", select "TucsonNodeService - alice.tucson.service" as the main class (libs/tucson.jar:libs/2p.jar is automatically added by Eclipse according to project's build path settings); then, right click on "t4jn_bookTrading.mas2j > Run Jason Application".

<a name="7">\[7\]</a> Separator `:` works on Mac & Linux only, use `;` on Windows.

---