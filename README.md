# Welcome to TuCSoN4Jason Repository

---

### What is TuCSoN4Jason?

**TuCSoN4Jason** (**t4jn** for short) is a *Java library* enabling *Jason* agents to exploit *TuCSoN* coordination services wrapped as Jason internal actions. **[Jason](http://http://jason.sourceforge.net/wp/)** is a well-known *Java-based* implementation of the *AgentSpeak(L)* language for *BDI agents*. **[TuCSoN](http://tucson.unibo.it)** is *Java-based* middleware providing software agents with coordination as a service via programmable logic tuple spaces, called *tuple centres*.

By combining TuCSoN and jason, t4jn aims at providing MAS engineers with a full-featured MAS middleware for cognitive agents, enabling them to exploit both dimensions of *agent-oriented software engineering* — individual, through Jason agents; social, via TuCSoN tuple centres — in a complete and well-balanced way.

t4jn is available under [GNU LGPL license](https://www.gnu.org/licenses/lgpl.html).

Main t4jn web page is <http://tucson4jason.apice.unibo.it/>.

---